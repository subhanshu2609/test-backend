<?php


use App\User;

class UsersTableSeeder extends DatabaseSeeder {
    public function run() {
        User::create([
            'name'           => 'Subhanshu',
            'role'           => 'admin',
            'student_number' => '1713128',
            'email'          => 'subhanshu.chaddha2@gmail.com',
            'phone_number'   => '9711635385',
            'branch'         => 'IT',
            'gender'         => 'male',
            'hosteler'       => false,
            'password'       => 'Subhanshu@'
        ]);
        User::create([
            'name'           => 'Siddhartha',
            'role'           => 'admin',
            'student_number' => '1713060',
            'email'          => 'imsidanand@gmail.com',
            'phone_number'   => '7275390010',
            'branch'         => 'IT',
            'gender'         => 'male',
            'hosteler'       => true,
            'password'       => 'Subhanshu@'
        ]);
        User::create([
            'name'           => 'Suchendra',
            'role'           => 'candidate',
            'student_number' => '1713120',
            'email'          => 'suchendra@test.com',
            'phone_number'   => '9999999999',
            'branch'         => 'IT',
            'gender'         => 'male',
            'hosteler'       => true,
            'password'       => 'secret'
        ]);
    }
}
