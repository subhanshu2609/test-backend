<?php

use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::unguard();
        $this->call(UsersTableSeeder::class);
//        $this->call(QuestionsTableSeeder::class);
//        $this->call(TestsTableSeeder::class);
        User::reguard();
    }
}
