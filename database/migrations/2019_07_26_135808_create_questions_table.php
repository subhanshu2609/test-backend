<?php

use App\Question;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->text('question');
            $table->enum('type', [
                Question::TYPE_C,
                Question::TYPE_C_PLUS_PLUS,
                Question::TYPE_JAVA,
                Question::TYPE_SQL,
                Question::TYPE_WEB,
                Question::TYPE_APTITUDE,
            ]);
            $table->integer('type_number');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
