<?php

use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->enum('role', [
                User::TYPE_ADMIN,
                User::TYPE_CANDIDATE
            ])->default(User::TYPE_CANDIDATE)->nullable();
            $table->string('student_number')->unique();
            $table->string('email')->unique();
            $table->string('phone_number')->unique();
            $table->string('branch');
            $table->enum('gender', [
                User::GENDER_MALE,
                User::GENDER_FEMALE
            ]);
            $table->boolean('hosteler');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }
}
