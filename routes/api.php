<?php

$api = app('Dingo\Api\Routing\Router');
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$basePath     = "App\Api\V1\Controllers\\";
$webhooksPath = $basePath . "Webhooks\\";

$api->version('v1', function (\Dingo\Api\Routing\Router $api) use ($basePath, $webhooksPath) {
    $api->get('test', function () {
        return 'PUBLIC_ROUTE';
    });

    // Auth Related
    $api->post('authenticate', $basePath . 'AuthController@authenticate');

    // Webhooks
    $api->group(['prefix' => 'webhooks'], function ($api) use ($webhooksPath) {
        // AWS
        $api->group(['prefix' => 'aws'], function ($api) use ($webhooksPath) {
            //
        });
    });
});


// All Authenticated Routes Goes Here.
$api->version('v1', ['middleware' => 'api.custom-auth'], function ($api) use ($basePath) {

    // Auth Related
    $api->get('questions/{type}', $basePath . 'QuestionController@index');
    $api->post('answers', $basePath . 'AnswerController@create');
    $api->post('responses', $basePath . 'ResponseController@create');

    //Admin:
    $api->version('v1', ['middleware' => 'api.auth.admin'], function ($api) use ($basePath) {

        $api->get('candidates', $basePath . 'UserController@index');
        $api->post('candidates', $basePath . 'UserController@create');

        $api->get('questions', $basePath . 'QuestionController@indexQuestions');
        $api->post('questions', $basePath . 'QuestionController@create');

        $api->get('result', $basePath . 'ResultController@index');

    });
});
