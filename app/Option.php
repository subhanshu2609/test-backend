<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Option
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Option newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Option newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Option query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $question_id
 * @property string $option
 * @property int $is_correct
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Option whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Option whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Option whereIsCorrect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Option whereOption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Option whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Option whereUpdatedAt($value)
 * @property-read \App\Question $question
 * @property int $choice
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Option whereChoice($value)
 * @property string $title
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Option whereTitle($value)
 */
class Option extends BaseModel {
    public function question() {
        return $this->belongsTo(Question::class);
    }
}
