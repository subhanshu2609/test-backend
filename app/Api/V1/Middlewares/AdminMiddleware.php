<?php

namespace App\Api\V1\Middlewares;

use App\Api\V1\Exceptions\Auth\NotAuthorizedException;
use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        $roles = [User::TYPE_ADMIN];

        if (Auth::user() && in_array(Auth::user()->role, $roles)) {
            return $next($request);
        }

        throw new NotAuthorizedException();
    }
}
