<?php


namespace App\Api\V1\Requests;


use App\Service\Contracts\IndexResultContract;

class IndexResultRequest extends BaseRequest implements IndexResultContract {
    const STUDENT_NO = 'student_number';

    public function rules() {
        return [
            self::STUDENT_NO => 'required|string'
        ];
    }

    public function getStudentNo() {
        return $this->get(self::STUDENT_NO);
    }
}
