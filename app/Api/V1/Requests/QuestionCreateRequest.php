<?php


namespace App\Api\V1\Requests;


use App\Service\Contracts\QuestionCreateContract;

class QuestionCreateRequest extends BaseRequest implements QuestionCreateContract {

    const TYPE     = 'type';
    const QUESTION = 'question';
    const OPTIONS  = 'options';

    public function rules() {
        return [
            self::TYPE                      => 'required|valid_question_types',
            self::QUESTION                  => 'required|string',
            self::OPTIONS                   => 'required|array',
            self::OPTIONS . '.*.title'     => 'required|string',
            self::OPTIONS . '.*.is_correct' => 'required|boolean',
        ];
    }

    public function getType() {
        return $this->get(self::TYPE);
    }

    public function getQuestion() {
        return $this->get(self::QUESTION);
    }

    public function getOptions() {
        return $this->get(self::OPTIONS);
    }
}
