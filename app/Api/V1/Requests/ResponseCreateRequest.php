<?php


namespace App\Api\V1\Requests;


use App\Service\Contracts\ResponseCreateContract;

class ResponseCreateRequest extends BaseRequest implements ResponseCreateContract {
    const INPUT = 'input';

    public function rules() {
        return [
            self::INPUT => 'nullable|string'
        ];
    }

    public function getInput() {
        return $this->get(self::INPUT);
    }
}
