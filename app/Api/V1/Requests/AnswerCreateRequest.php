<?php


namespace App\Api\V1\Requests;


use App\Service\Contracts\AnswerCreateContract;

class AnswerCreateRequest extends BaseRequest implements AnswerCreateContract {

    const ANSWERS = 'answers';

    public function rules() {
        return [
            self::ANSWERS                  => 'required|array',
            self::ANSWERS . '.*.question_id' => 'required',
            self::ANSWERS . '.*.option_id'   => 'required',
        ];
    }

    public function getAnswers() {
        return $this->get(self::ANSWERS);
    }
}
