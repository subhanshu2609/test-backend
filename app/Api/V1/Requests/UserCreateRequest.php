<?php


namespace App\Api\V1\Requests;


use App\Service\Contracts\UserCreateContract;

class UserCreateRequest extends BaseRequest implements UserCreateContract {
    const NAME           = 'name';
    const STUDENT_NUMBER = 'student_number';
    const EMAIL          = 'email';
    const PHONE_NUMBER   = 'phone_number';
    const BRANCH         = 'branch';
    const GENDER         = 'gender';
    const HOSTELER       = 'hosteler';
    const ROLE           = 'role';

    public function rules() {
        return [
            self::NAME           => 'required|string',
            self::STUDENT_NUMBER => 'required|numeric',
            self::EMAIL          => 'required|email',
            self::PHONE_NUMBER   => 'required|numeric',
            self::BRANCH         => 'required|string',
            self::GENDER         => 'required|valid_gender_types',
            self::HOSTELER       => 'required|boolean',
            self::ROLE           => 'nullable|valid_role_types'
        ];
    }

    public function hasRole() {
        return $this->has(self::ROLE);
    }
    public function getName() {
        return $this->get(self::NAME);
    }

    public function getStudentNumber() {
        return $this->get(self::STUDENT_NUMBER);
    }

    public function getEmail() {
        return $this->get(self::EMAIL);
    }

    public function getPhoneNumber() {
        return $this->get(self::PHONE_NUMBER);
    }

    public function getBranch() {
        return $this->get(self::BRANCH);
    }

    public function getGender() {
        return $this->get(self::GENDER);
    }

    public function getHosteler() {
        return $this->get(self::HOSTELER);
    }

    public function getRole() {
        return $this->get(self::ROLE);
    }
}
