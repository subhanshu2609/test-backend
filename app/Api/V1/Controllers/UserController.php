<?php

namespace App\Api\V1\Controllers;


use App\Api\V1\Requests\UserCreateRequest;
use App\Api\V1\Transformers\UserTransformer;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;

class UserController extends BaseController {

    public function showMe() {
        $user = Auth::user();

        return $this->response->item($user, new UserTransformer());
    }

    public function create(UserService $userService, UserCreateRequest $request) {
        $user = $userService->create($request);

        return $this->response->item($user, new UserTransformer());
    }

    public function index(UserService $userService) {
        $users = $userService->index();
        return $this->response->collection($users, new UserTransformer());
    }
}
