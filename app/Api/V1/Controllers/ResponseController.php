<?php


namespace App\Api\V1\Controllers;


use App\Api\V1\Requests\ResponseCreateRequest;
use App\Api\V1\Transformers\ResponseTransformer;
use App\Service\Entities\ResponseService;
use Illuminate\Support\Facades\Auth;

class ResponseController extends BaseController {

    public function create(ResponseService $responseService, ResponseCreateRequest $request) {
        $response = $responseService->create($request, Auth::user());
        return $this->response->item($response, new ResponseTransformer());
    }
}
