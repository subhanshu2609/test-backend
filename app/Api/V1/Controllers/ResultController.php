<?php


namespace App\Api\V1\Controllers;


use App\Api\V1\Requests\IndexResultRequest;
use App\Api\V1\Transformers\ResultTransformer;
use App\Service\Entities\ResultService;
use Illuminate\Support\Facades\Auth;

class ResultController extends BaseController {

    public function index(ResultService $resultService, IndexResultRequest $request) {
        $user = Auth::user();
        $result = $resultService->index($request);
        return $this->response->item($result, new ResultTransformer());
    }
}
