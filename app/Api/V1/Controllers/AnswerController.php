<?php


namespace App\Api\V1\Controllers;


use App\Api\V1\Requests\AnswerCreateRequest;
use App\Api\V1\Transformers\AnswerTransformer;
use App\Service\Entities\AnswerService;
use Illuminate\Support\Facades\Auth;

class AnswerController extends BaseController {

    public function create(AnswerService $answerService, AnswerCreateRequest $request) {

        $answers = $answerService->create($request, Auth::user());
        return $this->response->item($answers, new AnswerTransformer());
    }

}
