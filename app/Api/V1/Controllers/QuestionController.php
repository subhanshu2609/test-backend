<?php


namespace App\Api\V1\Controllers;


use App\Api\V1\Exceptions\UnauthorizedException;
use App\Api\V1\Requests\QuestionCreateRequest;
use App\Api\V1\Transformers\QuestionTransformer;
use App\Service\Entities\QuestionService;
use App\User;
use Illuminate\Support\Facades\Auth;

class QuestionController extends BaseController {
        public function create (QuestionService $questionService, QuestionCreateRequest $request) {
            $user = Auth::user();

            $question = $questionService->create($request, $user);
            return $this->response->item($question, new QuestionTransformer());
        }

        public function index (QuestionService $questionService, $type) {
            $questions = $questionService->index($type);
            return $this->response->collection($questions, new QuestionTransformer());
        }

        public function indexQuestions (QuestionService $questionService) {
            $questions = $questionService->indexQuestions();
            return $this->response->collection($questions, new QuestionTransformer());
        }
}
