<?php
/**
 * Created by PhpStorm.
 * User: piyushkantm
 * Date: 21/11/18
 * Time: 5:16 PM
 */

namespace App\Api\V1\Exceptions\Auth;


use App\Api\V1\Exceptions\ApiErrorCode;
use App\Api\V1\Exceptions\UnauthorizedException;

class NotAuthorizedException extends UnauthorizedException {
    const ERROR_MESSAGE = "You are not authorized to perform this action";

    public function __construct() {
        parent::__construct(self::ERROR_MESSAGE, ApiErrorCode::AUTH_NOT_AUTHORIZED);
    }
}
