<?php


namespace App\Api\V1\Transformers;


use App\Answer;
use League\Fractal\TransformerAbstract;

class AnswerTransformer extends TransformerAbstract {

    public function transform(Answer $answer) {
        return [
            'user_id'  => $answer->user_id,
            'response' => json_decode($answer->response, JSON_FORCE_OBJECT)
        ];
    }
}
