<?php

namespace App\Api\V1\Transformers;


use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract {
    public function transform(User $user) {
        return [
            'id'             => $user->id,
            'name'           => $user->name,
            'role'           => $user->role,
            'student_number' => $user->student_number,
            'email'          => $user->email,
            'branch'         => $user->branch,
            'phone_number'   => $user->phone_number,
            'gender'         => $user->gender,
            'hosteler'       => $user->hosteler
        ];
    }
}
