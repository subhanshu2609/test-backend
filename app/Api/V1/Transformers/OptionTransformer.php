<?php


namespace App\Api\V1\Transformers;


use App\Option;
use League\Fractal\TransformerAbstract;

class OptionTransformer extends TransformerAbstract {
    public function transform(Option $option) {
        return [
            'id'     => $option->id,
            'option' => $option->title,
        ];
    }
}
