<?php


namespace App\Api\V1\Transformers;


use App\Result;
use League\Fractal\TransformerAbstract;

class ResultTransformer extends TransformerAbstract {
    public function transform(Result $result) {
        return [
            'user_id'        => $result->user_id,
            'student_number' => $result->student_number,
            'name'           => $result->name,
            'email'          => $result->email,
            'score'          => $result->score
        ];
    }
}
