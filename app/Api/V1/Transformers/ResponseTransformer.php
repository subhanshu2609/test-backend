<?php


namespace App\Api\V1\Transformers;


use App\Question;
use App\Response;
use League\Fractal\TransformerAbstract;

class ResponseTransformer extends TransformerAbstract {

    public function transform(Response $response) {

        return [
            'id'      => $response->id,
            'user_id' => $response->user_id,
            'input'   => $response->input
        ];
    }
}
