<?php


namespace App\Api\V1\Transformers;

use App\Question;
use League\Fractal\TransformerAbstract;

class QuestionTransformer extends TransformerAbstract {

    protected $defaultIncludes = [
        'options'
    ];

    public function transform(Question $question) {

        return [
            'id'         => $question->id,
            'type'       => $question->type,
            'question'   => $question->question,
        ];
    }

    public function includeOptions(Question $question) {
        return $this->collection($question->options, new OptionTransformer());
    }
}
