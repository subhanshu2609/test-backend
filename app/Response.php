<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Response
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $input
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Response newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Response newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Response query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Response whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Response whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Response whereInput($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Response whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Response whereUserId($value)
 * @mixin \Eloquent
 */
class Response extends BaseModel
{
    public function user() {
        return $this->belongsTo(User::class);
    }
}
