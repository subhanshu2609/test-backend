<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Result
 *
 * @property int $id
 * @property int $user_id
 * @property string $student_number
 * @property string $name
 * @property string $email
 * @property int $score
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result whereStudentNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Result whereUserId($value)
 * @mixin \Eloquent
 */
class Result extends BaseModel {
    public function user() {
        return $this->get(User::class);
    }
}
