<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Question
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Question newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Question newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Question query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property string $question
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Question whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Question whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Question whereQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Question whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Question whereUserId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Option[] $options
 * @property-read \App\User $user
 * @property string $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Question whereType($value)
 * @property int $type_number
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Question whereTypeNumber($value)
 */
class Question extends BaseModel {

    const TYPE_C           = 'c';
    const TYPE_C_PLUS_PLUS = 'c++';
    const TYPE_JAVA        = 'java';
    const TYPE_SQL         = 'sql';
    const TYPE_WEB         = 'web';
    const TYPE_APTITUDE    = 'aptitude';

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function options() {
        return $this->hasMany(Option::class);
    }
}
