<?php


namespace App\Service\Entities;


use App\Option;
use App\Question;
use App\Service\Contracts\QuestionCreateContract;

class QuestionService {

    public function create(QuestionCreateContract $contract, $user) {
        $question           = new Question();

        $question->user_id  = $user->id;
        $question->question = $contract->getQuestion();
        $question->type     = $contract->getType();

        if ($question->type === Question::TYPE_C) {
            $type_number = 1;
        } elseif ($question->type === Question::TYPE_C_PLUS_PLUS){
            $type_number = 2;
        } elseif ($question->type == Question::TYPE_JAVA) {
            $type_number = 3;
        } elseif ($question->type == Question::TYPE_WEB) {
            $type_number = 4;
        } elseif ($question->type == Question::TYPE_SQL) {
            $type_number = 5;
        } elseif ($question->type == Question::TYPE_APTITUDE) {
            $type_number = 6;
        } else {
            $type_number = 7;
        }
        $question->type_number = $type_number;

        $question->save();

        $options = $contract->getOptions();

        foreach ($options as $option) {
            $optionObj              = new Option();
            $optionObj->question_id = $question->id;
            $optionObj->title       = $option['title'];
            $optionObj->is_correct  = $option['is_correct'];

            $optionObj->save();
        }

        return $question;
    }

    public function index($type) {

        $questions = Question::query();

        if($type === Question::TYPE_C) {
            $questions->whereNotIn('type', [Question::TYPE_C_PLUS_PLUS,Question::TYPE_JAVA]);
        }

        else if($type === Question::TYPE_C_PLUS_PLUS) {
            $questions->whereNotIn('type', [Question::TYPE_C,Question::TYPE_JAVA]);
        }

        if($type === Question::TYPE_JAVA) {
            $questions->whereNotIn('type', [Question::TYPE_C_PLUS_PLUS,Question::TYPE_C]);
        }

        return $questions->orderBy('type_number', 'asc')->get();


    }

    public function indexQuestions() {
        $questions = Question::orderBy('type_number', 'asc')->get();
        return $questions;
    }
}
