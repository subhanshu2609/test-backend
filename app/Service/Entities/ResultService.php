<?php


namespace App\Service\Entities;


use App\Answer;
use App\Option;
use App\Result;
use App\Service\Contracts\IndexResultContract;
use App\User;

class ResultService {

    public function index(IndexResultContract $contract) {
        $result = new Result();
        $score = 0;

        $student                = User::where('student_number', $contract->getStudentNo())->first();
        $result->user_id        = $student->id;
        $result->student_number = $student->student_number;
        $result->name           = $student->name;
        $result->email          = $student->email;

        $answers = Answer::where('user_id', $result->user_id)->first();
        $responses  = json_decode($answers->response, JSON_FORCE_OBJECT);
        foreach ($responses as $response) {
            $optionId = $response["option_id"];
            $option = Option::where('id', $optionId)->first();
            if($option->is_correct == 1) {
              $score++;
            }
        }
        $result->score = $score;
        $result->save();
        return $result;

    }
}
