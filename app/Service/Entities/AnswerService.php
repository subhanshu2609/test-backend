<?php


namespace App\Service\Entities;


use App\Answer;
use App\Service\Contracts\AnswerCreateContract;

class AnswerService {

    public function create(AnswerCreateContract $contract, $user) {
        $answers = new Answer();

        $answers->user_id  = $user->id;
        $answers->response = json_encode($contract->getAnswers(), JSON_FORCE_OBJECT);
        $answers->save();
        return $answers;
    }
}
