<?php

namespace App\Services;


use App\Service\Contracts\UserCreateContract;
use App\User;

class UserService {
    public function showUserById($userId) {
        return User::find($userId);
    }

    public function create(UserCreateContract $contract) {
        $candidate                 = new User();
        $candidate->name           = $contract->getName();
        $candidate->student_number = $contract->getStudentNumber();
        $candidate->email          = $contract->getEmail();
        $candidate->phone_number   = $contract->getPhoneNumber();
        $candidate->branch         = $contract->getBranch();
        $candidate->gender         = $contract->getGender();
        $candidate->hosteler       = $contract->getHosteler();
        $candidate->password       = $contract->getStudentNumber();

        if ($contract->hasRole()) {
            $candidate->role = $contract->getRole();
        }
        $candidate->save();
        return $candidate;
    }

    public function index() {
        return User::orderBy('name', 'asc')
            ->get();
    }
}
