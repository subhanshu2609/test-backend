<?php


namespace App\Service\Entities;


use App\Response;
use App\Service\Contracts\ResponseCreateContract;

class ResponseService {

    public function create(ResponseCreateContract $contract, $user) {
        $response = new Response();

        $response->user_id = $user->id;
        $response->input = $contract->getInput();
        $response->save();
        return $response;
    }
}
