<?php


namespace App\Service\Contracts;


interface UserCreateContract {

    public function hasRole();

    public function getName();

    public function getStudentNumber();

    public function getEmail();

    public function getPhoneNumber();

    public function getBranch();

    public function getGender();

    public function getHosteler();

    public function getRole();
}
