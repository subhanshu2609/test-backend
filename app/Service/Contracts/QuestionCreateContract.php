<?php


namespace App\Service\Contracts;


interface QuestionCreateContract {

    public function getType();

    public function getQuestion();

    public function getOptions();

}
