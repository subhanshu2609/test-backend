<?php


namespace App\Service\Contracts;


interface AnswerCreateContract {

    public function getAnswers();
}
